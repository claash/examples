$('.accordion__item label').click(function() {
	if ($(this).parent().hasClass('open')) {
		$(this).parent().removeClass('open');
		return;
	}

	var _this = this;
	$(this).parents('.accordion').find('.accordion__item').removeClass('open');

	setTimeout(function() {
		$(_this).parent().toggleClass('open');
	}, 100);
});
