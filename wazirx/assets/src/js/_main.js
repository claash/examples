// img svg to inline svg
function svgInline() {
	$('img.svg').each(function() {
		var $img = $(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		$.get(
			imgURL,
			function(data) {
				// Get the SVG tag, ignore the rest
				var $svg = $(data).find('svg');

				// Add replaced image's ID to the new SVG
				if (typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if (typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass + ' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Replace image with new SVG
				$img.replaceWith($svg);
			},
			'xml'
		);
	});
}

svgInline();

// open menu on mobile
$('.header button').click(function() {
	$(this).parent().find('nav').toggleClass('show');
});

// ref slider
$('.ref-howit__slider').on('init', function() {
	svgInline();
});

$('.ref-howit__slider').slick({
	arrows: false,
	slidesToShow: 3,
	responsive: [
		{
			breakpoint: 992,
			settings: {
				centerMode: true,
				slidesToShow: 1,
				slideToScroll: 1,
				infinite: false,
				arrows: true,
				dots: true,
				prevArrow:
					'<button type="button" class="slick-prev"><img class="svg" src="./assets/dist/img/arrowPrev.svg"></button>',
				nextArrow:
					'<button type="button" class="slick-next"><img class="svg" src="./assets/dist/img/arrowNext.svg"></button>'
			}
		}
	]
});

// custom checkbox
$('.custom-checkbox').click(function() {
	$(this).toggleClass('checked');
	var checkbox = $(this).find('input[type="checkbox"]');
	checkbox.prop('checked', !checkbox.prop('checked'));
});

// form validation
function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

var messages = {
	email: 'Invalid email',
	pass: {
		short: 'Password length should be at least 6',
		note: 'Passwords mismatch'
	},
	terms: 'Please agree to the terms'
};

function errorAdd(elem, message) {
	elem.parent().remove('passed');
	elem.parent().addClass('error');
	elem.parent().find('span.message').remove();
	elem.parent().append('<span class="message">' + message + '</span>');
}

function errorRem(elem, message) {
	elem.parent().removeClass('error');
	elem.parent().addClass('passed');
	elem.parent().find('span.message').remove();
}

$('form.valid').submit(function(e) {
	// email validation
	var email = $(this).find('input[type="email"]');

	if (!validateEmail(email.val())) {
		errorAdd(email, messages.email);
		e.preventDefault();
	} else {
		errorRem(email);
	}

	// pass validation
	$(this).find('input[type="password"]').each(function() {
		if ($(this).val().trim().length < 6) {
			errorAdd($(this), messages.pass.short);
			e.preventDefault();
		} else {
			errorRem($(this));
		}
	});

	// pass compate
	var passwords = $(this).find('input[type="password"]');
	if (passwords.length == 2) {
		if ($(passwords[0]).val() !== $(passwords[1]).val()) {
			errorAdd($(passwords[0]), messages.pass.note);
			errorAdd($(passwords[1]), messages.pass.note);
			e.preventDefault();
		}
	}

	// custom checkbox validation
	if ($(this).find('.custom-checkbox').length) {
		if (!$(this).find('.custom-checkbox input').prop('checked')) {
			errorAdd($(this).find('.custom-checkbox input'), messages.terms);
			e.preventDefault();
		} else {
			errorRem($(this).find('.custom-checkbox input'));
		}
	}
});
