var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	autoprefixerOptions = {
		browsers: [ 'last 2 versions' ]
	},
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	browserSync = require('browser-sync'),
	fileinclude = require('gulp-file-include');

// Compile sass into CSS & auto-inject into browsers
gulp
	.src('./node_modules/slick-carousel/slick/fonts/*.{ttf,woff,woff2,eof,eot,svg}')
	.pipe(gulp.dest('./assets/dist/fonts/slick'));

gulp.task('sass', () => {
	return gulp
		.src('./assets/src/sass/app.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
		.pipe(gulp.dest('./assets/dist/css'))
		.pipe(autoprefixer(autoprefixerOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./assets/dist/css'))
		.pipe(browserSync.stream());
});

// Move the javascript files into our /src/js folder
gulp.src('./node_modules/jquery/dist/jquery.min.js').pipe(gulp.dest('./assets/dist/js/'));

gulp.task('js', () => {
	return gulp
		.src('./assets/src/js/app.js')
		.pipe(
			fileinclude({
				prefix: '@@',
				basepath: '@file'
			})
		)
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./assets/dist/js'));
});

// HTML
gulp.task('html', () => {
	return gulp
		.src([ './html/*.html' ])
		.pipe(
			fileinclude({
				prefix: '@@',
				basepath: '@file'
			})
		)
		.pipe(gulp.dest('./'));
});

// Static Server + watching scss/html files
gulp.task(
	'serve',
	gulp.series('sass', function() {
		browserSync.init({
			server: './'
		});

		gulp.watch([ './assets/src/sass/*.scss', './assets/src/sass/**/*.scss' ], gulp.series('sass'));
		gulp.watch([ './html/*.html', './html/**/*.html' ], gulp.series('html')).on('change', browserSync.reload);
		gulp.watch('./assets/src/js/*.js', gulp.series('js')).on('change', browserSync.reload);
	})
);

gulp.task('default', gulp.parallel('sass', 'js', 'html', 'serve'));
