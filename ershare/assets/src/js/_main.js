/* MAIN CODE */

// lazy load img
$('.lazy').lazy({
	effect: 'fadeIn',
	effectTime: 500
});

// home stocks slider
$('.home-stocks').slick({
	arrows: false,
	//centerMode: true,
	variableWidth: true,
	//infinite: true,
	autoplay: true
});

// home hero slider
$('.home-hero--right').slick({
	arrows: false,
	slidesToShow: 1,
	dots: true,
	infinite: true,
	draggable: false,
	autoplay: true
});

// home posts slider
$('.home-posts__slider').slick({
	arrows: false,
	variableWidth: true,
	infinite: true,
	dots: true,
	slidesToScroll: 2,
	slidesToShow: 5,
	responsive: [
		{
			breakpoint: 576,
			settings: {
				slidesToScroll: 1,
				slidesToShow: 1
			}
		}
	]
});

// home news slider
$('.home-news__slider').slick({
	arrows: false,
	variableWidth: true,
	infinite: true,
	dots: true,
	centerMode: true
});

// header menu
$('.header button').click(function() {
	$(this).toggleClass('open');
	$('.content').toggleClass('menu-show');

	if ($('.header nav').hasClass('open')) {
		$('.header nav').css('transition-delay', '0s, 0s');
		$('body').css('overflow', 'auto');
	} else {
		$('.header nav').css('transition-delay', '0s, .3s');
		$('body').css('overflow', 'hidden');
	}

	$('.header nav').toggleClass('open');
});

// cut quote home hero
$('.home-hero--right blockquote p').each(function() {
	$(this).text($(this).text().slice(0, 108) + ' ...');
});
