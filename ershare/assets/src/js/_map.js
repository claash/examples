var gStyle = [
	{
		elementType: 'geometry',
		stylers: [
			{
				color: '#f5f5f5'
			}
		]
	},
	{
		elementType: 'labels.icon',
		stylers: [
			{
				visibility: 'off'
			}
		]
	},
	{
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#616161'
			}
		]
	},
	{
		elementType: 'labels.text.stroke',
		stylers: [
			{
				color: '#f5f5f5'
			}
		]
	},
	{
		featureType: 'administrative.land_parcel',
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#bdbdbd'
			}
		]
	},
	{
		featureType: 'poi',
		elementType: 'geometry',
		stylers: [
			{
				color: '#eeeeee'
			}
		]
	},
	{
		featureType: 'poi',
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#757575'
			}
		]
	},
	{
		featureType: 'poi.park',
		elementType: 'geometry',
		stylers: [
			{
				color: '#e5e5e5'
			}
		]
	},
	{
		featureType: 'poi.park',
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#9e9e9e'
			}
		]
	},
	{
		featureType: 'road',
		elementType: 'geometry',
		stylers: [
			{
				color: '#ffffff'
			}
		]
	},
	{
		featureType: 'road.arterial',
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#757575'
			}
		]
	},
	{
		featureType: 'road.highway',
		elementType: 'geometry',
		stylers: [
			{
				color: '#dadada'
			}
		]
	},
	{
		featureType: 'road.highway',
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#616161'
			}
		]
	},
	{
		featureType: 'road.local',
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#9e9e9e'
			}
		]
	},
	{
		featureType: 'transit.line',
		elementType: 'geometry',
		stylers: [
			{
				color: '#e5e5e5'
			}
		]
	},
	{
		featureType: 'transit.station',
		elementType: 'geometry',
		stylers: [
			{
				color: '#eeeeee'
			}
		]
	},
	{
		featureType: 'water',
		elementType: 'geometry',
		stylers: [
			{
				color: '#c9c9c9'
			}
		]
	},
	{
		featureType: 'water',
		elementType: 'labels.text.fill',
		stylers: [
			{
				color: '#9e9e9e'
			}
		]
	}
];

var contentString =
	'<div id="content">' +
	'<div id="siteNotice">' +
	'</div>' +
	'<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
	'<div id="bodyContent">' +
	'<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
	'sandstone rock formation in the southern part of the ' +
	'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) ' +
	'south west of the nearest large town, Alice Springs; 450&#160;km ' +
	'(280&#160;mi) by road. Kata Tjuta and Uluru are the two major ' +
	'features of the Uluru - Kata Tjuta National Park. Uluru is ' +
	'sacred to the Pitjantjatjara and Yankunytjatjara, the ' +
	'Aboriginal people of the area. It has many springs, waterholes, ' +
	'rock caves and ancient paintings. Uluru is listed as a World ' +
	'Heritage Site.</p>' +
	'<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
	'https://en.wikipedia.org/w/index.php?title=Uluru</a> ' +
	'(last visited June 22, 2009).</p>' +
	'</div>' +
	'</div>';

function initMap() {
	var center = { lat: 42.3532355, lng: -71.0586047 };

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 16,
		center: center,
		styles: gStyle,
		disableDefaultUI: true
	});

	marker = new google.maps.Marker({
		position: new google.maps.LatLng(42.3532355, -71.0586047),
		map: map,
		icon: './assets/dist/img/marker.svg'
	});

	var contentData = '';

	var contentString = '<div class="google-map__info">' + contentData + '</div>';

	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});

	infowindow.open(map, marker);

	marker.addListener('click', function() {
		infowindow.open(map, marker);
	});
}
